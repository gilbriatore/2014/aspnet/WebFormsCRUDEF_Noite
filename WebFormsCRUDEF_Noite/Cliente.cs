﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebFormsCRUDEF_Noite
{
    public class Cliente
    {
        public int Id { get; set; }
        public string Nome { get; set; }
        public string Telefone { get; set; }
    }
}