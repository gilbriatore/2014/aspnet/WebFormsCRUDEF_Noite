﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebFormsCRUDEF_Noite
{
    public partial class Cadastrar : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnGravar_Click(object sender, EventArgs e)
        {
            Contexto ctx = new Contexto();
            Cliente c = new Cliente();

            c.Nome = txtNome.Text;
            c.Telefone = txtTelefone.Text;

            ctx.Clientes.Add(c);
            ctx.SaveChanges();

            txtNome.Text = "";
            txtTelefone.Text = "";
        }
    }
}